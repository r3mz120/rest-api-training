const express = require('express')
const router = express.Router()
const usersRouter = require('./users')
const otherRouter = require('./other') //Ruta pentru resetul bazei de date

router.use('/', otherRouter)
router.use('/', usersRouter)

module.exports = router
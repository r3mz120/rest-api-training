const express = require("express")
const router = express.Router();
const UserDB = require('../models').User

router.get('/users', async (req, res) => {
    UserDB.findAll({
            attributes: ['user_name', 'email']
        }).then(users => {
            res.status(200).send(users)
        })
        .catch(() => {
            res.status(500).send({
                message: "Eroare baza de date"
            })
        })
})

router.post('/register', async (req, res) => {
    const errors = []

    const user = {
        user_name: req.body.user_name,
        email: req.body.email,
        phone: req.body.phone,
        password: req.body.password
    }

    if (!user.user_name) {
        errors.push('Nu ai introdus numele utilizatorului')
    } else if (!user.user_name.match(/([A-ZAÎ??Â])+(?=.{1,40}$)[a-zA-ZAÎ??Âaî??]+(?:[-\\s][a-zA-ZAÎ??Âaî??â]+)*$/)) {
        errors.push('Invalid user name!')
    }

    if (!user.email) {
        errors.push('Nu ai introdus email-ul!')
    } else if (!user.email.match(/[a-zA-Z0-9_\.-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9\.]{2,5}$/)) {
        errors.push('Format invalid email!')
    }

    if (!user.phone) {
        errors.push('Nu ai introdus numarul de telefon!')
    } else if (!user.phone.match(/^(\+4|)?(07[0-8]{1}[0-9]{1}|02[0-9]{2}|03[0-9]{2}){1}?(\s|\.|\-)?([0-9]{3}(\s|\.|\-|)){2}$/)) {
        errors.push('Formatul numarului de telefon este invalid!')
    }

    if (!user.password) {
        errors.push('Nu ai introdus parola')
    } else if (!user.password.match(/^.{4,40}$/)) {
        errors.push('Parola trebuie sa contina intre 4 si 40 de caractere')
    }

    if (errors.length === 0) {
        try {
            await UserDB.create(user)
            res.status(201).send({
                message: 'Utilizator adaugat!'
            })
        } catch (error) {
            console.log(error);
            res.status(500).send({
                message: 'Eroare la crearea unei utilizator!'
            })
        }
    } else {
        res.status(400).send({
            errors
        })
    }

})

router.put('/update/:id', async (req, res) => {
    try {
        const user = await UserDB.findOne({
            where: {
                id: req.params.id
            }
        })
        if (user) {
            if (!user.password) {
                res.status(400).send({
                    message: "Nu ai introdus parola noua"
                })
            } else if (!user.password.match(/^.{4,40}$/)) {
                res.status(400).send({
                    message: "Parola trebuie sa aiba intre 4 si 40 de caractere"
                })
            } else {
                user.update({
                        password: req.body.password
                    })
                    .then(() => {
                        res.status(200).send({
                            message: "Ok, utilizator updatat"
                        })
                    })
            }
        }
    } catch {
        res.status(500).send({
            message: 'Server crapat'
        })
    }
})
module.exports = router;
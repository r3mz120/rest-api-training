const db = require('../config/db') // Preia configuratia bazei de date din fisierul config
const User = db.import('./users') // Importare in baza de date a tabelul din fisierul users

module.exports = { //Export pentru reutilizare
    User,
    connection: db
}
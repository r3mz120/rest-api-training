module.exports = (sequelize, DataTypes) => {
    return sequelize.define('user', {
        user_name: DataTypes.STRING,
        email: DataTypes.STRING,
        phone: DataTypes.STRING,
        password: DataTypes.STRING
    }, {
        underscored: true
    })
}
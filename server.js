const express = require('express') // Importare framework express intr-o variabila constanta
const bodyParser = require('body-parser')
const router = require('./routes')
const app = express(); // Initializare instanta express pentru aplicatie
const port = 8080

app.use(bodyParser.json()) // Orice request trece prin body parser

app.get('/', (req, res) => {
    res.status(200).send('Serveru merge')
})

app.use('/api', router) // Serveste in functie de requestul facut

app.listen(port, () => {
    console.log('Serverul ruleaza pe portul: ' + port)
})